/*
Navicat MySQL Data Transfer

Source Server         : Aram
Source Server Version : 100125
Source Host           : localhost:3306
Source Database       : aram

Target Server Type    : MYSQL
Target Server Version : 100125
File Encoding         : 65001

Date: 2017-12-17 21:29:10
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for friends
-- ----------------------------
DROP TABLE IF EXISTS `friends`;
CREATE TABLE `friends` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `u1` int(11) NOT NULL,
  `u2` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `df` (`u1`),
  KEY `sdff` (`u2`),
  CONSTRAINT `df` FOREIGN KEY (`u1`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sdff` FOREIGN KEY (`u2`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of friends
-- ----------------------------
INSERT INTO `friends` VALUES ('10', '13', '7');
INSERT INTO `friends` VALUES ('13', '13', '2');
INSERT INTO `friends` VALUES ('14', '13', '12');
INSERT INTO `friends` VALUES ('15', '13', '11');
INSERT INTO `friends` VALUES ('16', '13', '5');
INSERT INTO `friends` VALUES ('17', '13', '13');
INSERT INTO `friends` VALUES ('18', '13', '7');
INSERT INTO `friends` VALUES ('19', '7', '14');

-- ----------------------------
-- Table structure for messages
-- ----------------------------
DROP TABLE IF EXISTS `messages`;
CREATE TABLE `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender` int(11) NOT NULL,
  `resiver` int(11) NOT NULL,
  `text` longtext NOT NULL,
  `time` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `dsfaaaaaaaaaa` (`sender`),
  KEY `dfshjadddddddd` (`resiver`),
  CONSTRAINT `dfshjadddddddd` FOREIGN KEY (`resiver`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `dsfaaaaaaaaaa` FOREIGN KEY (`sender`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of messages
-- ----------------------------
INSERT INTO `messages` VALUES ('27', '13', '7', 'Barev', '12-17 12:08');
INSERT INTO `messages` VALUES ('28', '13', '7', 'Barev', '12-17 12:24');
INSERT INTO `messages` VALUES ('29', '13', '7', 'Vonces', '12-17 12:24');
INSERT INTO `messages` VALUES ('32', '7', '13', 'sdfsdf', '12-17 14:27');
INSERT INTO `messages` VALUES ('35', '13', '7', 'Barev', '12-17 12:33');
INSERT INTO `messages` VALUES ('37', '7', '13', 'sfd', '12-17 12:42');
INSERT INTO `messages` VALUES ('38', '13', '7', 'Baylus', '12-17 12:55');
INSERT INTO `messages` VALUES ('39', '13', '7', 'asdasd', '12-17 01:19');
INSERT INTO `messages` VALUES ('40', '13', '14', 'Barev', '12-17 05:31');
INSERT INTO `messages` VALUES ('41', '13', '7', 'Barev', '12-17 07:28');

-- ----------------------------
-- Table structure for nkarner
-- ----------------------------
DROP TABLE IF EXISTS `nkarner`;
CREATE TABLE `nkarner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sfdfdw` (`user_id`),
  CONSTRAINT `sfdfdw` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of nkarner
-- ----------------------------
INSERT INTO `nkarner` VALUES ('21', '13', 'Lighthouse.jpg');
INSERT INTO `nkarner` VALUES ('22', '13', '133.jpg');
INSERT INTO `nkarner` VALUES ('23', '13', '13254406_479234808942281_7780752624804268504_n.jpg');
INSERT INTO `nkarner` VALUES ('24', '14', '1111.jpg');
INSERT INTO `nkarner` VALUES ('25', '7', '1111.jpg');
INSERT INTO `nkarner` VALUES ('26', '7', '13254406_479234808942281_7780752624804268504_n.jpg');
INSERT INTO `nkarner` VALUES ('27', '7', '133.jpg');
INSERT INTO `nkarner` VALUES ('28', '7', 'Lighthouse.jpg');
INSERT INTO `nkarner` VALUES ('29', '13', 'Jellyfish.jpg');

-- ----------------------------
-- Table structure for posts
-- ----------------------------
DROP TABLE IF EXISTS `posts`;
CREATE TABLE `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `vernagir` varchar(255) NOT NULL,
  `text` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `asdas` (`user_id`),
  CONSTRAINT `asdas` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of posts
-- ----------------------------
INSERT INTO `posts` VALUES ('9', '13', 'fadss', 'fsdf');
INSERT INTO `posts` VALUES ('10', '14', 'Ashun', 'Shat xonav ashun e');
INSERT INTO `posts` VALUES ('11', '7', 'asd', 'sadasd');

-- ----------------------------
-- Table structure for request
-- ----------------------------
DROP TABLE IF EXISTS `request`;
CREATE TABLE `request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uxarkox` int(11) NOT NULL,
  `yndunox` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fsd` (`uxarkox`),
  KEY `dsf` (`yndunox`),
  CONSTRAINT `dsf` FOREIGN KEY (`yndunox`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sdasd` FOREIGN KEY (`uxarkox`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of request
-- ----------------------------
INSERT INTO `request` VALUES ('6', '13', '7');
INSERT INTO `request` VALUES ('7', '5', '7');
INSERT INTO `request` VALUES ('10', '13', '7');
INSERT INTO `request` VALUES ('11', '13', '7');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `age` int(11) NOT NULL,
  `ser` varchar(255) NOT NULL,
  `photo` varchar(255) NOT NULL,
  `friend` varchar(255) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'Aram', 'Abrahamyan', 'Abrahamyan2001aram@gmail.com', '455531aram', '16', '', '', '0');
INSERT INTO `users` VALUES ('2', 'asdasddsad', 'asd', 'Abrahamyanasd', 'sadasdasd', '14', '', '', '0');
INSERT INTO `users` VALUES ('3', 'sda', 'sadas', 'Abrahamyan2001aram@gmail.com', 'sadasd', '14', '', '', '0');
INSERT INTO `users` VALUES ('5', 'sadasdada', 'sad', 'Abrahamyan@mail.ru', 'c99a11a53a3748269e3f86d7ac38df11', '14', '', '', '0');
INSERT INTO `users` VALUES ('6', 'Sad', 'asd', 'asdasdasd@gmail.com', '$2y$10$.EdUvsXyWC4.RBPNc557EuE9Cnk5/WqTNO6dSICxfk5MrDET1hEpG', '2011', 'man', '', '0');
INSERT INTO `users` VALUES ('7', 'Sad', 'asd', 'aram@mail.ru', '$2y$10$efPgt49ojVXyBNLx3ApTQuJf4ay7wR.sP3cqn.qIM2Jb6lAD0jZUa', '2011', 'man', 'Penguins.jpg', '0');
INSERT INTO `users` VALUES ('11', 'sdf', 'sdf', 'sdfsdf@mail.ru', 'asdkashdf', '18', 'man', '', '0');
INSERT INTO `users` VALUES ('12', 'dsad', 'sadasd', 'asdas@mail.ru', '$2y$10$1WJmwSWQAMvcj3Z5RPkmMOsf3uNxkELKTy6WmIxvfSeeHI6Os5hGu', '2011', 'man', '', '0');
INSERT INTO `users` VALUES ('13', 'Aram', 'Abrahamyan', 'aram2@mail.ru', '$2y$10$8to8Nk8Uf3BG2npg8G/Tz.G23j5OTP1Xv3SAhLHBjtV6EAQISgnf2', '2011', 'man', '1111.jpg', '0');
INSERT INTO `users` VALUES ('14', 'Arev', 'Poghosyan', 'arevik@mail.ru', '$2y$10$QMDES8knVpmz6zxonxvTH.Fei0TdxbAwWc9lGbsU5EniaJpYPvbv.', '1982', 'woman', '133.jpg', '0');

-- ----------------------------
-- Procedure structure for chack_age
-- ----------------------------
DROP PROCEDURE IF EXISTS `chack_age`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `chack_age`(x int(11))
begin 
declare tariq int(11);
set tariq = (select age from users where id = x);
if tariq < 18 then
  delete from users where id = x;
  end if; 
end
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for filtrate
-- ----------------------------
DROP PROCEDURE IF EXISTS `filtrate`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `filtrate`(a int(11),b int(11))
begin 
 declare x int(11);
 set x = (select count(*) from friends where (u1 = a and u2 = b) or (u1 = b and u2 = a));
 if x = 0 THEN
 delete from messages where (sender = a and resiver = b) or (sender = b and resiver = a);
 end if;
 end
;;
DELIMITER ;
DROP TRIGGER IF EXISTS `t2`;
DELIMITER ;;
CREATE TRIGGER `t2` AFTER INSERT ON `messages` FOR EACH ROW begin
call filtrate(new.sender, new.resiver);
end
;;
DELIMITER ;
DROP TRIGGER IF EXISTS `t1`;
DELIMITER ;;
CREATE TRIGGER `t1` AFTER INSERT ON `users` FOR EACH ROW begin 
call chack_age(new.id);

end
;;
DELIMITER ;
SET FOREIGN_KEY_CHECKS=1;
