<?php
session_start();
if(!isset($_SESSION['user'])) {
	header('location:index.php');
}
?>

<!DOCTYPE html>
<html>
<head>
	<title></title>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="CSS/profile.css">
   
</head>
<body>

<!-- The Modal -->
<div class="modal fade" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Ընկերներ</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>











<div id="glux">
  <div id="glxavor1">

      <div id="searchdiv">
    <input type="text" id="search">
    <img src="images/search.png" class="search_png">
  </div>
  <div id="box"></div>
  <img src="images/hayter.png" id="hayter" data-id="<?= $_SESSION['user'][0]['id'] ?>">
  <img src="images/home.png" id="home">

  <div id="hayter_div">

  </div>
  </div>
</div>
<div id="marmin">
  <div id="glxavor2">
    <div id="verev">

          <div>

      <?php if (empty($_SESSION['user'][0]['photo'])): ?>
        <img src="images/page.jpg" id="nkar">
      <?php else: ?>
        <img src="<?= 'images/'.$_SESSION['user'][0]['photo'] ?>" id="nkar">
      <?php endif; ?>




      <form id="avatar" action="server.php" method="post" enctype="multipart/form-data">
        <input type="file" id="nkar2" name="nkar">
        <input type="hidden" name="action" value="upload_photo">
      </form>
    </div>
           <h2 class="h2"><?php
           echo  $_SESSION['user'][0]['name'] . ' '. $_SESSION['user'][0]['surname']
       ?>
       </h2>
       <div id="dashter">
         <h4 class="friend dashter" data-toggle="modal" data-target="#myModal">Ընկերներ</h4>
         <h4 class="photos dashter">Նկարներ</h4>
         <h4 class="posts dashter">Հայտարարություններ</h4>
       </div>
          <a href="index.php?logout" class="btn btn-info logout" id="log">Logout</a>
    </div>
    <div id="nerqev">
    <div id="avel"><h3 id="a1">Ավելացրեք ձեր հայտարարությունը</h3></div>
      <div id="post">
        <h2 >Հայտարարության վերնագիր</h2>
        <input type="text" class="inp_v form-control" id="vernagir">
        <h3 >Հայտարարություն</h3>
        ՙ<textarea class="textarea form-control" ></textarea>
        <button id="send_post" class="btn btn-success">Կիսվել</button>
      </div>
    </div>
  </div>
</div>
<div id="messages">
  <img id="sms_img">
  <h4 id="anun"> </h4>
  <img src="images/close.png" id="close">
  <div id="namakner"></div>
  <input type="text" id="namak" class="form-control">
  <button id="send_sms" class="btn btn-success">Ուղարկել</button>
</div>

<div id="nkarner">

<div id="nkarner1">
<h1 style="margin-left: 70px;">Ավելացնել Լուսանկար</h1>
<img id='add_nkar' src='images/add.png'>
<form action="server.php" method="post" id="form" enctype="multipart/form-data">
<input type="file" name="nkar2" id="add_photo">
<input type="hidden" name="action" value="upload_photo_profile">
<button id="add_btn">Ավելացնել</button>
<h4 style="margin-left: 70px;">Լուսանկարը ջնջելու համար 2 անգամ սեղմեք նկարի վրա:</h4 style="margin-left: 20px;">
</form>
</div>
<div id="nkarner2"></div>
<div id="posts"></div>
</div>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>

<script type="text/javascript" src="js/profile.js">
  
	</script>
</html>